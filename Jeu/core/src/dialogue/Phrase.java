package dialogue;

import Items.Item;
import Items.Pomme;

public class Phrase {
	
	private String texte;
	private int redirect;
	private Item cadeau;
	
	public Phrase(String t, int r) {
		if(!t.contains("KDO")) {
			this.texte = t;
		}else {
			if( t.substring(t.indexOf("KDO")+3, t.length()).equals("pomme") )
				this.cadeau = new Pomme("pomme");
			this.texte = t.substring(0,t.indexOf("KDO"));
		}
		this.redirect = r;
	}
	
	public String getTexte() {
		return this.texte;
	}
	
	public int getRedirect() {
		return this.redirect;
	}
	
	public Item getKdo() {
		return this.cadeau;
	}

}
