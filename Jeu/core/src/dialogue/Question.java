package dialogue;

import java.util.ArrayList;

public class Question extends Phrase{
	
	private ArrayList<Phrase> options;
	private int select;
	
	public Question(String t, int r, String[] o){
		super(t, r);
		this.select = 0;
		this.options = new ArrayList<Phrase>();
		for(int i=1;i<o.length;i++) {
			this.options.add(new Phrase(o[i].substring(0, o[i].length()-2),
					Integer.parseInt(o[i].substring(o[i].length()-2,o[i].length()))));
		}
	}
	
	public Phrase getOptions(int o) {
		return this.options.get(o);
	}
	
	public ArrayList<Phrase> getAllOptions() {
		return this.options;
	}
	
	public int getSelect() {
		return this.select;
	}
	
	// 3 = droite, 1 = gauche
	public void setSelect(int i) {
		if(i==3) 
			if(this.select < this.options.size())
				this.select++;
		if(i==1)
			if(this.select > 0)
				this.select--;
	}

}
