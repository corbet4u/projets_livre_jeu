package dialogue;

public class DialogueFight extends Phrase{
	
	private int win, loose;
	
	public DialogueFight(int i, int i2) {
		super("FIGHT", 0);
		this.win = i;
		this.loose = i2;
	}
	
	public int getWin() {
		return this.win;
	}
	
	public int getLoose() {
		return this.loose;
	}

}
