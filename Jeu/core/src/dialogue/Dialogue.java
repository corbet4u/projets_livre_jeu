package dialogue;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import aRanger.Evenement;
import aRanger.PNJ;
import aRanger.Player;

public class Dialogue extends Evenement{
	
	private ArrayList<Phrase> texte;
	private int enCours;
	private Sprite image;
	private PNJ pnj;
	private boolean fini;
	private Player player;
	
	/* Le constructeur prend en parametre tout les dialogues/question d un PNJ*/
	public Dialogue(ArrayList<String> s, PNJ p, Player player) {
		this.texte = new ArrayList<Phrase>();
		this.player = player;
		// on parcourt tout les "ecrans" de dialogue
		for( int i=0; i<s.size(); i++ ) {
			int i2 = s.get(i).length();
			// on verifie si l "ecran" est une question ( prefixe "QQ" )
			if(s.get(i).substring(0, 2).equals("QQ")) {
				// si c est le cas on split la question pour recuperer toutes les options ( question sous forme : QQquestion?/option1/option2 )
				String[] tmp = s.get(i).split("/");
				// et on cree/ajoute la question au dialogue 
				this.texte.add(new Question(tmp[0].substring(2, tmp[0].length()),99,tmp));
			}else {
				if(s.get(i).substring(0, 5).equals("FIGHT")) {
					String[] tmp = s.get(i).split("/");
					this.texte.add(new DialogueFight(Integer.parseInt(tmp[1]),Integer.parseInt(tmp[2])));
				}else {
					// sinon on ajoute la phrase au dialogue
					this.texte.add(new Phrase(s.get(i).substring(0, i2-2),Integer.parseInt(s.get(i).substring(i2-2, i2))));
				}
			}
		}
		// on initialise l etat d avancement du dialogue a 0 ( au debut quoi )
		enCours = 0;
		// on initialise la texture de la boite de dialogue
		this.image = new Sprite(new Texture("sprite/dialogue.png"));
		this.pnj = p;
		this.fini = false;
	}
	
	/* Methode qui est appelee pour parcourir les options dans une direction
	 * 0 = haut, 1 = gauche, 2 = bas, 3 = droite */
	public void toucheDir(int i) {
		// on verifie que l ecran de dialogue en cours soit une question
		if(this.texte.get(enCours) instanceof Question) 
			// si oui on change l options en cours de selection ( l option sur laquelle la fleche est posee ) 
			((Question)this.texte.get(enCours)).setSelect(i);
	}
	
	/* Methode qui permet de continuer le dialogue a la prochaine phrase/boite si il en reste, renvoie false si le dialogue est termine */
	public void continuer() {
		// si l ecran de dialogue est une question
		if( this.texte.get(this.enCours) instanceof Question ) {
			// on avance le dialogue en fonction de la reponse donnee
			Question q = ((Question)this.texte.get(this.enCours));
			this.enCours = q.getOptions(q.getSelect()).getRedirect();
			if(this.texte.get(this.enCours).getKdo() != null)
				this.player.recevoir(this.texte.get(this.enCours).getKdo());
		}else {
			// si il reste du dialogue
			if( this.texte.get(this.enCours).getRedirect() != 00 ) {
				// on avance dans le dialogue
				this.enCours = this.texte.get(this.enCours).getRedirect();
				if(this.texte.get(this.enCours).getKdo() != null)
					this.player.recevoir(this.texte.get(this.enCours).getKdo());
			}
			else
				this.fini = true;
		}
	}
	
	public void apresCombat( boolean win ) {
		if( win ) 
			this.enCours = ((DialogueFight)this.texte.get(this.enCours)).getWin();
		else
			this.enCours = ((DialogueFight)this.texte.get(this.enCours)).getLoose();
	}
	
	/* Methode qui dessine la boite pour les dialogues , les dialogues ainsi que le nom du PNJ */
	public void draw(SpriteBatch sb) {
		// on dessine la boite de dialogue
		sb.draw(this.image, 15, 15, Gdx.graphics.getWidth()-30, Gdx.graphics.getHeight()/2);
		// on recupere le texte a afficher
		String str = this.texte.get(enCours).getTexte();
		BitmapFont font = new BitmapFont();
		// on affiche le nom du PNJ avec une plus grosse ecriture
		font.getData().setScale(2);
		font.draw(sb, this.pnj.getName(), 80, Gdx.graphics.getHeight()/2-50);
		// on affiche le texte du dialogue avec une ecriture plus petite
		font.getData().setScale(1.4f);
		font.draw(sb, str, 50, Gdx.graphics.getHeight()/2-100);
		// si l ecran de dialogue en cours est une question
		if(this.texte.get(enCours) instanceof Question) {
			int y = 70, x = 250;
			// on recupere la liste des options
			ArrayList<Phrase> options = ((Question)this.texte.get(enCours)).getAllOptions();
			// on parcours les options en les affichant a la suite en les decalant vers la droite
			for(int i=0;i<options.size();i++) {
				// si l option en cours d affichage est celle est en cours de selection
				if( i == ((Question)this.texte.get(enCours)).getSelect() )
					// on affiche la fleche a sa gauche
					sb.draw(new Sprite(new Texture("sprite/fleche.png")), x-60, y-25);
				font.draw(sb,options.get(i).getTexte(),x,y);
				x += 100;
			}
		}
	}
	
	public String getTexte() {
		return this.texte.get(this.enCours).getTexte();
	}
	
	public String getNom() {
		return this.pnj.getName();
	}
	
	public PNJ getPNJ() {
		return this.pnj;
	}
	
	public boolean getFini() {
		return this.fini;
	}

}
