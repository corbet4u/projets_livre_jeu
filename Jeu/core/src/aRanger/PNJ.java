package aRanger;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;

public class PNJ {
	
	private Sprite sprite;
	private Position position;
	private MapObject obj;
	private String name;
	private int pv;
	private int attaque;
	
	public PNJ(MapObject m) {
		this.obj = m;
		this.sprite = new Sprite(new Texture("sprite/"+m.getProperties().get("text").toString()));
		this.position = new Position(((RectangleMapObject)m).getRectangle().getX() , ((RectangleMapObject)m).getRectangle().getY());
		this.name = m.getProperties().get("nomPNJ").toString();
		this.pv = Integer.parseInt(m.getProperties().get("pv").toString());
		this.attaque = Integer.parseInt(m.getProperties().get("attaque").toString());
	}
	
	/* Methode qui permet de savoir si le PNJ se trouve utilise l espace a un endroit donne */
	public boolean colision(float x, float y) {
		if( x > this.getX() && x < (this.getX()+this.sprite.getWidth()*Play.MULTIPLICATEUR) && y > this.getY() && y < (this.getY()+this.sprite.getWidth()*Play.MULTIPLICATEUR))
			return true;
		return false;
	}
	
	/* Methode qui permet de savoir si ce PNJ se trouve oui ou non devant quelque chose (a une position dans une direction donn�e, la porte est de 20pixel) */
	public boolean estdevant(float x, float y, int direction) {
		float m = Play.MULTIPLICATEUR;
		switch(direction) {
			case 0:
				if( x > this.getX()  && x < (this.getX()+this.sprite.getWidth()*m) && (y+25*m) > this.getY() && (y+25*m) < this.getY() )
					return true;
			case 1:
				if( (x-25*m) > this.getX() && (x-25*m) < (this.getX()+this.sprite.getWidth()*m) && y > this.getY() && y < (this.getY()+this.sprite.getWidth()*m) )
					return true;
			case 2:
				if( x > this.getX()  && x < (this.getX()+this.sprite.getWidth()*m) && (y-25*m) > this.getY() && (y-25*m) < this.getY() )
					return true;
			case 3:
				if( (x+25*m) > this.getX() && (x+25*m) < (this.getX()+this.sprite.getWidth()*m) && y > this.getY() && y < (this.getY()+this.sprite.getWidth()*m) )
					return true;
		}
		return false;
	}
	
	public String getDialogue() {
		return this.obj.getProperties().get("dialogue").toString();
	}
	
	public String getCombat() {
		return this.obj.getProperties().get("combat").toString();
	}
	
	public void draw(SpriteBatch sb) {
		sb.draw(this.sprite, this.getX(), this.getY(), sprite.getWidth()*Play.MULTIPLICATEUR, sprite.getHeight()*Play.MULTIPLICATEUR);
	}
	
	public void perdrePv(int i) {
		this.pv -= i;
		if(this.pv < 0)
			this.pv = 0;
	}
	
	public float getX() {
		return this.position.getX();
	}
	
	public float getY() {
		return this.position.getY();
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getPv() {
		return this.pv;
	}
	
	
	public int getAttaque() {
		return this.attaque;
	}
	
	public void setPosition(float x, float y) {
		this.position.setPosition(x, y);
	}

}
