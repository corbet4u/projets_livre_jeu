package aRanger;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import dialogue.Dialogue;

public class Evenement {
	
	public void draw(SpriteBatch sb) {
		this.draw(sb);
	}
	
	public boolean verifCombat() {
		if( this instanceof Dialogue ) 
			if( ((Dialogue)this).getTexte().equals("FIGHT") ) 
				return true;
		return false;
	}
	
	public boolean combatfini() {
		if( this instanceof Combat )
			if( ((Combat)this).fini() )
				return true;
		return false;
	}
	
	public boolean dialoguefini() {
		if( this instanceof Dialogue )
			return ((Dialogue)this).getFini();
		return false;
	}
	
	public void interagir() {
		if( this instanceof Combat )
			((Combat)this).update();
		else if( this instanceof Dialogue )
				((Dialogue)this).continuer();
	}
	
	public void toucheDir(int i) {
		this.toucheDir(i);
	}

}
