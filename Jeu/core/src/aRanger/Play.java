package aRanger;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;

import dialogue.Dialogue;

public class Play implements Screen {
	
	private Map map;
	private OrthogonalTiledMapRenderer renderer;
	private OrthographicCamera camera;
	private Evenement event;
	public static float MULTIPLICATEUR = 1;
	
	private Player player;
	
	// equivalent du create
	public void show() {
		// creation de la map, du render et de la camera
		map = new Map("map");
		renderer = new OrthogonalTiledMapRenderer(map.getMap());
		camera = new OrthographicCamera();
		
		// creation du hero et du controlleur
		this.player = new Player(map);
		Gdx.input.setInputProcessor(new Controller(this));
		
		// positionnement de la camera en fonction de la taille de la map
		camera.position.set(map.getMap().getProperties().get("width", Integer.class)*map.getMap().getProperties().get("tilewidth", Integer.class)/2,
				map.getMap().getProperties().get("height", Integer.class)*map.getMap().getProperties().get("tileheight", Integer.class)/2,0);
		camera.update();
		
	}

	// equivalent du update + render
	public void render(float delta) {
		if( MULTIPLICATEUR != Gdx.graphics.getWidth() / ((float)1024) ) 
			MULTIPLICATEUR = Gdx.graphics.getWidth() / ((float)1024);
		
		// "Nettoyage" de l ecran 
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// reglage de la camera et affichage de la map
		renderer = new OrthogonalTiledMapRenderer(map.getMap(),Gdx.graphics.getWidth()/(map.getMap().getProperties().get("width",Integer.class)*32f));
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
		camera.update();
		renderer.setView(camera);
		renderer.render();
		
		// demarrage et recuperation du render pour les sprites
		renderer.getBatch().begin();
		SpriteBatch sb = (SpriteBatch)renderer.getBatch();

		// affichage du hero
		this.map.draw(this.player,sb);
		if(this.event == null) {
			if(this.player.getEvent() == null) {
				// alors on laisse le hero se mettre a jour en fonction de la commande
				this.player.update();
			}else {
				this.event = this.player.getEvent();
				this.player.clearEvent();
				this.event.draw(sb);
			}
		}else {
			if ( this.event.combatfini() ) {
				boolean b = ((Combat)this.event).win();
				this.event = ((Combat)this.event).getDiag();
				((Dialogue)this.event).apresCombat(b);
			}
			if ( this.event.verifCombat() )
				this.event = new Combat((Dialogue)this.event, this.player, ((Dialogue)this.event).getPNJ());
			if ( this.event.dialoguefini() )
				this.event = null;
			if( this.event != null )
				this.event.draw(sb);	
		}
		//Si on doit ouvrir l'inventaire
		if(this.player.getInventaire().getAfficheInventaire())
			this.player.getInventaire().draw(sb);		//On le dessine
		
		// on ferme le render
		renderer.getBatch().end();
	}
	
	public void combat() {
		this.player.combat();
	}
	
	/* Methode qui permet au joueur d interagir avec les PNJ si aucun dialogue n est en cours
	 * Permet egalement de continuer dans le dialogue si il reste du texte
	 * Ou simplement de finir le dialogue si il est termine */
	public void interagir() {
		if( this.event != null )
			this.event.interagir();
		else
			this.player.interagir();
	}
	
	/* Methode appelee par le controlleur en fonction de la direction tapee au clavier */
	public void toucheDir(int i) {
		if(this.event == null) {
			this.player.setDir(i); 
			this.player.setMoving(true);
		}else 
			this.event.toucheDir(i);
	}
	
	@Override
	public void resize(int width, int height) {
		if((width*9/16) > 1000) {
			DisplayMode dm = Gdx.graphics.getDisplayMode();
			Gdx.graphics.setFullscreenMode(dm);
		}else {
			Gdx.graphics.setWindowedMode(width, width*9/16);
		}
		camera.viewportWidth = width;
		camera.viewportHeight = width*9/16;
		camera.update();
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		map.getMap().dispose();
	}

	// equivalent du quit
	public void dispose() {
		map.getMap().dispose();
		renderer.dispose();
	}
	
	public Player getplayer() {
		return this.player;
	}
	
	public void test() {
		TiledMapTileLayer layer = (TiledMapTileLayer)map.getMap().getLayers().get("calque 1");
		Cell cell = new Cell();
		TiledMapTileSet tileSet = map.getMap().getTileSets().getTileSet("tileset 1");
		cell.setTile(tileSet.getTile(9)); 
		layer.setCell(2, 2, cell); 
		this.map.test(layer);
	}

	public void inventaire() {
		this.player.getInventaire().setAfficheInventaire();
	}
}
