package aRanger;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TmxMapLoader.Parameters;

import Items.Item;
import Items.Pomme;

public class Map {
	
	private TiledMap map;
	private TiledMapTileLayer calque1;
	private ArrayList<PNJ> pnj;
	private ArrayList<Item> items;
	
	public Map(String nmap) {
		Parameters params = new Parameters();
		params.textureMinFilter = TextureFilter.Linear;
		params.textureMagFilter = TextureFilter.Nearest;
		map = new TmxMapLoader().load("maps/"+nmap+".tmx", params);
		calque1 = ((TiledMapTileLayer) map.getLayers().get("calque 1"));
		
		this.pnj = new ArrayList<PNJ>();
		this.items = new ArrayList<Item>();
		
		// on recupere tout les objets de type PNJ dans une liste de PNJ de la map
		for( int i=0; i < this.map.getLayers().get("PNJ").getObjects().getCount(); i++ ) 		
			this.pnj.add(new PNJ(this.map.getLayers().get("PNJ").getObjects().get(i)));
		
		for( int i=0; i < this.map.getLayers().get("Item").getObjects().getCount(); i++ ){	
			if(this.map.getLayers().get("Item").getObjects().get(i).getProperties().get("genre").toString().equals("pomme"))
					this.items.add(new Pomme(this.map.getLayers().get("Item").getObjects().get(i)));
		}
		
	}
	
	/* Methode qui permet de savoir si il y a colision ou non a un endroit precis */
	public boolean colision(float x, float y) {
		boolean b = false;
		int i = 0, j =0;
		int tailleX = Gdx.graphics.getWidth()/32;
		int tailleY = Gdx.graphics.getHeight()/18;
		
		// on cherche le numero de ligne et colonne de la case sur laquelle le personnage va se trouver
		while(i*tailleX < x)
			i++;
		while(j*tailleY < y)
			j++;
		
		for( int ii=0; ii<this.pnj.size(); ii++ )
			if(this.pnj.get(ii).colision(x,y))
				b = true;
		
		// on test ensuite la solidite de cette case
		if( this.calque1.getCell(i-1, j-1).getTile().getProperties().containsKey("blocked") || b )
			return true;
		return false;	
	}
	
	public void test(TiledMapTileLayer t) {
		this.calque1 = t;
	}
	
	/* Methode qui verifie si le joueur est dans un objet et effectue l action assignee si c est le cas*/
	public void trigger(Player p) {
		int i = 0;
		boolean b = false;
		RectangleMapObject mp;
		// on parcours tout les objets de type "trigger" sur la map en testant si le joueur se trouve dans l un d eux 
		while (i < this.map.getLayers().get("trigger").getObjects().getCount() && !b){
			mp = ((RectangleMapObject) this.map.getLayers().get("trigger").getObjects().get(i));
			if( (p.getX()+15) > mp.getRectangle().getX() && (p.getX()+15) < (mp.getRectangle().getX() + mp.getRectangle().getWidth()) && 
				p.getY()+24 > mp.getRectangle().getY() && p.getY()+24 < (mp.getRectangle().getY() + mp.getRectangle().getHeight())) {
				// si oui on regarde le type de l objet
				if( this.map.getLayers().get("trigger").getObjects().get(i).getProperties().get("type").toString().equals("teleport") ) {
					// si c est un objet du type "teleport" on change de map 
					this.changeMap(this.map.getLayers().get("trigger").getObjects().get(i).getProperties().get("NMap").toString());
					p.setMap(this);
					if ( p.getX() < 100 ) p.setX(850); 
					else if ( p.getX() > 850 ) p.setX(50); 
					//if ( p.getY() < 100 ) p.setY(360); 
					//if ( p.getY() > 850 ) p.setY(35);
					b = true;
				}
			}else {
				i++;
			}
		}
	}
	
	/* Methode qui dessine tout les PNJ ainsi que le hero */
	public void draw(Player p,SpriteBatch sb) {
		
		for( int i=0;i<this.items.size();i++) {
				this.items.get(i).draw(sb);
		}
		// on affiche les PNJ se trouvant derriere le hero
		for( int i=0;i<this.pnj.size();i++) {
			if( this.pnj.get(i).getY() > p.getY() )
				this.pnj.get(i).draw(sb);
		}
		// on affiche le hero
		p.draw(sb);
		// on affiche les PNJ se trouvant devant le hero
		for( int i=0;i<this.pnj.size();i++) {
			if( this.pnj.get(i).getY() <= p.getY() )
				this.pnj.get(i).draw(sb);
		}
	}
	
	/* Methode qui charge la map en cas de changement */
	public void changeMap(String nmap) {
		this.map = new TmxMapLoader().load("maps/"+nmap+".tmx");
		this.calque1 = ((TiledMapTileLayer) this.map.getLayers().get("calque 1"));
	}
	
	/* Methode qui verifie si y a un PNJ a un endroit precis ( dans une direction donnee ) */ 
	public PNJ qqndevant(float x, float y, int direction) {
		for(int i=0;i<this.pnj.size();i++)
			if(this.pnj.get(i).estdevant(x,y,direction))
				return this.pnj.get(i);
		return null;
	}
	
	
	public Item itemPresent(float x, float y, int direction) {
		boolean b = false;
		for(int i=0; i<this.items.size(); i++)
			if(this.items.get(i).estDessous(x, y)){
				b = true; 									
				return this.items.get(i);
			}
		if(!b)
			for(int i=0; i<this.items.size(); i++)
				if(this.items.get(i).estDevant(x, y, direction))
					return this.items.get(i);
		return null;
	}
	
	public void suppItem(Item i){
		this.items.remove(i);
	}
	
	public TiledMap getMap() {
		return map;
	}

}
