package aRanger;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import inventaire.Inventaire;

public class Controller implements InputProcessor{
	
	private Play play;
	
	public Controller(Play p) {
		this.play = p;
	}

	/* Methode appelee quand une touche est pressee*/
	public boolean keyDown(int keycode) {
		Inventaire inventaire = this.play.getplayer().getInventaire();
		
		switch(keycode) {
			case Keys.Z : this.play.toucheDir(0); break;
			case Keys.Q : this.play.toucheDir(1); break;
			case Keys.S : this.play.toucheDir(2); break;
			case Keys.D : this.play.toucheDir(3); break;
			case Keys.E : 
				if(inventaire.getAfficheInventaire())
					inventaire.utiliserItem(inventaire.getItemSelect(), this.play.getplayer());
				else
					this.play.interagir(); 
				break;
			case Keys.F : this.play.combat(); break;
			case Keys.H : this.play.test(); break;
			
			//Inventaire
			case Keys.I : this.play.inventaire(); break;     //Ouverture/fermeture inventaire
			case Keys.RIGHT : if(inventaire.getAfficheInventaire()) inventaire.navig_DG(1); break;   //Navig inventaire Droite
			case Keys.LEFT : if(inventaire.getAfficheInventaire()) inventaire.navig_DG(-1); break;   //Navig inventaire Gauche
			case Keys.UP : if(inventaire.getAfficheInventaire()) inventaire.navig_DG(-8); break;   //Navig inventaire Haut
			case Keys.DOWN : if(inventaire.getAfficheInventaire()) inventaire.navig_DG(8); break;   //Navig inventaire Bas
			case Keys.Y : if(inventaire.getAfficheInventaire()) inventaire.jeterItem(); break;   //Navig inventaire Gauche
			
		}
		return false;
	}

	/* Methode appelee quand une touche est relachee */
	public boolean keyUp(int keycode) {
		this.play.getplayer().setMoving(false);
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* Methodes inutiles pour le moment ( verification de touche appuye / souris / tactile ) */

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
