package aRanger;

public class Stat {
	
	private int attaque, defense, pv, xp, lv;
	
	public Stat() {
		this.pv = 50;
	}
	
	public void perdrePv(int i) {
		this.pv -= i;
		if(this.pv < 0)
			this.pv = 0;
		System.out.println(this.pv);
	}
	
	public int getPv() {
		return this.pv;
	}

}
