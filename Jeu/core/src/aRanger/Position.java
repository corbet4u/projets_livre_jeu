package aRanger;

public class Position {
	
	private float x,y;
	
	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setPosition(float x, float y) {
		this.x = x/Play.MULTIPLICATEUR;
		this.y = y/Play.MULTIPLICATEUR;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getX() {
		return this.x*Play.MULTIPLICATEUR;
	}
	
	public float getY() {
		return this.y*Play.MULTIPLICATEUR;
	}

}
