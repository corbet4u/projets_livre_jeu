package aRanger;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import Items.Item;
import dialogue.Dialogue;
import dialogue.DialogueFight;
import inventaire.Inventaire;

public class Player {
	
	private Map map;
	private boolean moving;
	private int direction;
	private Animation[] anim;
	private float temps;
	private Sprite tr;
	private Position position;
	private Evenement event;
	private Inventaire inventaire;
	private ArrayList<Attaque> attaques;
	private Stat stat;
	
	public Player(Map t) {
		this.map = t;
		this.direction = 0;
		this.temps = 0;
		this.anim = new Animation[4];
		this.loadAnimation();
		this.position = new Position(200, 300);
		this.tr = new Sprite((TextureRegion)this.anim[this.direction].getKeyFrame(0));
		this.inventaire = new Inventaire();
		this.stat = new Stat();
		
		this.attaques = new ArrayList<Attaque>();
		this.attaques.add(new Attaque("test", 10));
		this.attaques.add(new Attaque("test 2", 10));
	}
	
	public void update() {
		// verification de la presence d un trigger ( piege, teleport, etc )
		this.map.trigger(this);
		// deplacement si possible du hero
		this.deplacement();
		// evolution de l animation
		this.calculAnim();
	}
	
	/* Methode qui deplace le joueur si c est possible ( si pas de colision )*/
	public void deplacement() {
		float futurX = this.getX(), futurY = this.getY();
		// on calcule la futur position en fonction de la direction
		if(this.moving) {
			switch (this.direction) {
	        	case 0: futurY += (1f * Gdx.graphics.getDeltaTime())*150*Play.MULTIPLICATEUR; break;
		        case 1: futurX -= (1f * Gdx.graphics.getDeltaTime())*150*Play.MULTIPLICATEUR; break;
		        case 2: futurY -= (1f * Gdx.graphics.getDeltaTime())*150*Play.MULTIPLICATEUR; break;
		        case 3: futurX += (1f * Gdx.graphics.getDeltaTime())*150*Play.MULTIPLICATEUR; break;
	        }
			// on test si il y a colision ou non
			float tmp = tr.getWidth()*Play.MULTIPLICATEUR;
			if( !(this.map.colision(futurX, futurY) || this.map.colision(futurX+tmp, futurY) || this.map.colision(futurX, futurY+tmp) || this.map.colision(futurX+tmp, futurY+tmp)) )
				this.position.setPosition(futurX, futurY);
		}
	}
	
	/* Methode qui calcul le sprite a afficher */
	public void calculAnim() {
		temps += Gdx.graphics.getDeltaTime();  
		// on choisit le sprite a afficher en fonction de si le personnage est en mouvement ou non et du temps passe
		if(!this.moving) {
			//this.tr = (TextureRegion)this.anim[this.direction].getKeyFrame(0);
			this.tr = new Sprite((TextureRegion)this.anim[0].getKeyFrame(0));
			temps = 0;
		}else {
			this.tr = new Sprite((TextureRegion)this.anim[0].getKeyFrame(temps,true));
		}
	}
	
	/* Methode qui permet d interagir si un PNJ se trouve devant lui ( dans la direction ou il regarde a moins de 20pixel 
	 * On peut aussi ramasser un item par terre (en dessous ou en face)*/
	public void interagir() {
		boolean b = false;
		Item item = this.map.itemPresent(this.getX()+15, this.getY()+15, this.direction);
		//AJOUT DANS LINVENTAIRE
		if(item != null){
			this.inventaire.ajoutItem(item);
			this.map.suppItem(item);
			return;
		}
		else {
			PNJ inter = this.map.qqndevant(this.getX()+15, this.getY()+15, this.direction);
			if ( inter != null ) 
				this.event = this.creerDialogue(inter, 1);
		}
	}
	
	/* choix 1 = dialogue, choix 2 = dialogue de combat*/
	public Dialogue creerDialogue(PNJ inter, int choix) {
		String[] l;
		if(choix == 1)
			l = inter.getDialogue().split(";");
		else
			l = inter.getCombat().split(";");
		ArrayList<String> liste = new ArrayList<String>();
		for( int i=0; i<l.length; i++ )
			liste.add(l[i]);
		return new Dialogue(liste, inter, this);
	}
	
	public void combat() {
		PNJ inter = this.map.qqndevant(this.getX()+15, this.getY()+15, this.direction);
		if ( inter != null ) 
			this.event = this.creerDialogue(inter, 2);
	}
	
	/* Methode qui affiche le hero */
	public void draw(SpriteBatch sb) {
		sb.draw(tr,this.position.getX(),this.position.getY(),tr.getWidth()*Play.MULTIPLICATEUR,tr.getHeight()*Play.MULTIPLICATEUR);
	}
	
	/* Methode qui charge tout les sprites du personnages et decide du temps d animation */
	public void loadAnimation() {
		Texture t = new Texture("sprite/perso2.png");
		// TextureRegion[][] tmp = TextureRegion.split(t, t.getWidth() / 9 , t.getHeight() / 4);
		TextureRegion[][] tmp = TextureRegion.split(t, 30 , 47);
		int ss = 0;
		TextureRegion tmp2[] = new TextureRegion[9];
		for(int j=0;j<4;j++) {
			for(int i=0;i<9;i++) {
				tmp2[ss] = tmp[j][i];
				ss++;
			}
			anim[j] = new Animation(1f / tmp2.length, tmp2);
			tmp2 = new TextureRegion[9];
			ss = 0;
		}
	}
	
	public void recevoir(Item i) {
		this.inventaire.ajoutItem(i);
	}
	
	public Stat getStat() {
		return this.stat;
	}
	
	public void setPosition(float x, float y) {
		this.position.setPosition(x, y);
	}
	
	public void setMap(Map t) {
		this.map = t;
	}
	
	public void setMoving(boolean b) {
		this.moving = b;
	}
	
	public void setDir(int d) {
		this.direction = d;
	}
	
	public void setX(float x) {
		this.position.setX(x);
	}
	
	public void setY(float y) {
		this.position.setY(y);
	}
	
	public float getX() {
		return this.position.getX();
	}
	
	public float getY() {
		return this.position.getY();
	}
	
	public Inventaire getInventaire(){
		return this.inventaire;
	}
	
	public Evenement getEvent() {
		return this.event;
	}
	
	public ArrayList<Attaque> getAttack(){
		return this.attaques;
	}
	
	public void clearEvent() {
		this.event = null;
	}
}
