package aRanger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import dialogue.Dialogue;

public class Combat extends Evenement{
	
	private boolean fini;
	private Sprite sprite;
	private Dialogue diag;
	private Player player;
	private PNJ mechant;
	private int attackSelect;
	
	public Combat(Dialogue di, Player p, PNJ m) {
		this.sprite = new Sprite(new Texture("sprite/vs.png"));
		this.fini = false;
		this.diag = di;
		this.mechant = m;
		this.player = p;
		this.attackSelect = 0;
	}
	
	public void update() {
		this.mechant.perdrePv(this.player.getAttack().get(this.attackSelect).getDgt());
		System.out.println(this.player.getAttack().get(this.attackSelect).getDgt()+" degats faits");
		System.out.println("reste "+this.mechant.getPv());
		
		if(this.mechant.getPv() == 0)
			this.fini = true;
		else
			this.player.getStat().perdrePv(this.mechant.getAttaque());
		
		if(this.player.getStat().getPv() == 0)
			this.fini = true;
		
		System.out.println("Vous perdez "+this.mechant.getAttaque()+" pv");
		System.out.println("Il vous reste "+this.player.getStat().getPv()+" pv");
	}
	
	public void draw(SpriteBatch sb) {
		sb.draw(this.sprite, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		sb.draw(new Sprite(new Texture("sprite/dialogue.png")), 10, 10, Gdx.graphics.getWidth()-20, Gdx.graphics.getHeight()/3);
		
		BitmapFont font = new BitmapFont();
		font.getData().setScale(1.4f);
		int x = 50;
		for(int i=0; i<this.player.getAttack().size();i++) {
			font.draw(sb, this.player.getAttack().get(i).getName(), x, Gdx.graphics.getHeight()/3-100);
			x+=200;
		}
		sb.draw(new Sprite(new Texture("sprite/fleche.png")), -10+(200*this.attackSelect), Gdx.graphics.getHeight()/3-125);
	}
	
	public void toucheDir(int i) {
		switch(i) {
		case 1: 
			if( this.attackSelect > 0 )
				this.attackSelect--;
			break;
		case 3: 
			if( this.attackSelect < this.player.getAttack().size()-1 )
				this.attackSelect++;
			break;
		}
	}
	
	public boolean fini() {
		return this.fini;
	}
	
	public boolean win() {
		if(this.mechant.getPv() == 0)
			return true;
		return false;
	}
	
	public Dialogue getDiag() {
		return this.diag;
	}

}
