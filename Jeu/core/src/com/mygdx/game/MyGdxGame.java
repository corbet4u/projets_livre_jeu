package com.mygdx.game;

import com.badlogic.gdx.Game;

import aRanger.Play;

public class MyGdxGame extends Game {

	
	@Override
	public void create () {
		setScreen(new Play());
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
	}
}
