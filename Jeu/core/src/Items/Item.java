package Items;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;

import aRanger.Play;
import aRanger.Player;
import aRanger.Position;

public class Item {

	private Position pos;	
	private String nom;
	private Sprite sprite;
	private String description;
	private Integer dura;
	
	public Item(MapObject mp){
		this.sprite = new Sprite(new Texture("sprite/"+mp.getProperties().get("text").toString()));
		this.pos = new Position(((RectangleMapObject)mp).getRectangle().getX(), ((RectangleMapObject)mp).getRectangle().getY());
		this.nom = mp.getProperties().get("genre").toString();
		this.description = "TO DO";
		this.dura = 2;	 // A mettre sur la Tiled
	}
	
	public Item(String nom, String sprite, String d, int du) {
		this.nom = nom;
		this.sprite = new Sprite(new Texture("sprite/"+sprite));
		this.description = d;
		this.dura = du;
	}
	
	public boolean estDessous(float x, float y){
		if( x > this.getX()-10  && x < (this.getX()+this.sprite.getWidth()+10) && y > this.getY()-10 && y < this.getY()+this.sprite.getHeight()+10 )
			return true;
		return false;
	}
	
	public boolean estDevant(float x, float y, int direction){
		switch(direction) {
		case 0:
			if( x > this.getX()  && x < (this.getX()+this.sprite.getWidth()) && (y+20) > this.getY() && (y+20) < this.getY() )
				return true;
		case 1:
			if( (x-20) > this.getX() && (x-20) < (this.getX()+this.sprite.getWidth()) && y > this.getY() && y < (this.getY()) )
				return true;
		case 2:
			if( x > this.getX()  && x < (this.getX()+this.sprite.getWidth()) && (y-20) > this.getY() && (y-20) < this.getY() )
				return true;
		case 3:
			if( (x+20) > this.getX() && (x+20) < (this.getX()+this.sprite.getWidth()) && y > this.getY() && y < (this.getY()) )
				return true;
		}
		return false;
	}

	public void utiliser(Player p) {
		System.out.println(this);
		this.dura--;
		if(this.dura == 0) 
			p.getInventaire().jeterItem(this);
		
	}
	
	public void draw(SpriteBatch sb) {
		sb.draw(this.sprite, this.getX(), this.getY(), sprite.getWidth()*Play.MULTIPLICATEUR, sprite.getHeight()*Play.MULTIPLICATEUR);
	}
	
	public float getX() {
		return this.pos.getX();
	}
	
	public float getY() {
		return this.pos.getY();
	}
	
	public String getName() {
		return this.nom;
	}
	
	public Sprite getSprite(){
		return this.sprite;
	}
	
	public void setPosition(float x, float y) {
		this.pos.setPosition(x, y);
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public int getDura() {
		if(this.dura != null)
			return this.dura;
		else 
			return 1;
	}
	
	public void setDura(int d) {
		this.dura = d;
	}
}
