package Items;

import com.badlogic.gdx.maps.MapObject;

import aRanger.Player;

public class Pomme extends Item{

	public Pomme(MapObject mp) {
		super(mp);
	}
	
	public Pomme(String nom) {
		super(nom,"pomme.png", "Une pomme rouge qui rend 5pv", 2);
	}

	public void utiliser(Player p) {
		p.getStat().perdrePv(-5);
		super.utiliser(p);
	}
	
}
