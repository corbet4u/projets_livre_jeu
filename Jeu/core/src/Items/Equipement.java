package Items;

import aRanger.Player;

public abstract class Equipement extends Item{
	
	private String description;
	private int resistance;
	private String type; //casque, torse, bottes, jambe
	private final static String[] EQUIP = {"casque", "torse", "bottes", "jambe"};
	
	public Equipement(String n, String t, String d, int r, String sp) {
		//On v�rifie que le type d'�quipement est valide, sinon on le d�finie comme �tant un casque
		super(n, sp, d, -1);
		boolean tmp = false;
		for(int i = 0; i < EQUIP.length; i++)
			if(this.EQUIP[i].contentEquals(t.toLowerCase())) 
				tmp = true;
		if(tmp)
			this.type = t;
		else
			this.type = "casque";
		this.resistance = r;
	}
	
	public void utiliser(Player p) {
		p.getInventaire().equipItem(this);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getResistance() {
		return resistance;
	}

	public void setResistance(int resistance) {
		this.resistance = resistance;
	}
	
	public String getType() {
		return this.type;
	}
	
}
