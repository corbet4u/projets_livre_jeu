package inventaire;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Items.Casque_Fer;
import Items.Equipement;
import Items.Item;
import Items.Pomme;
import aRanger.Play;
import aRanger.Player;
import aRanger.Position;

public class Inventaire {

	
	private ArrayList<Item> items;					//Contient tous les items de l'inventaire
	private HashMap<String, Equipement> armure;		//Contient les �quipements �quip�s, il existe les key "casque, torse, jambe, bottes"
	private Sprite sprite;
	private Sprite spriteEquipement;
	private boolean afficheInventaire; 
	private Position pos;
	
	private Inventaire_naviguation navig;
	
	public final static int TAILLE = 24;					//
	private final static int largeurInventaire = 8;			// Valeurs changeables en fonction du sprite de l'inventaire
	private final static int hauteurInventaire = 3;			//
	private boolean toDel = true;		//Utiliser pour faire test sur inventaire, a del dans le futur
	
	
	public Inventaire(){
		this.sprite = new Sprite(new Texture("sprite/Inventaire.png"));
		this.spriteEquipement = new Sprite(new Texture("sprite/Inventaire_Equipement.png"));
		this.items = new ArrayList<Item>();
		this.armure = new HashMap<String, Equipement>();
		this.afficheInventaire = false;
		this.navig = new Inventaire_naviguation(this);
		this.pos = new Position(200,200);
	}
	
	/**
	 * M�thode qui premet l'ajout d'un item
	 * @param item
	 * @return true si il y avait de la place, sinon false
	 */
	public boolean ajoutItem(Item item){
		if(toDel) {										//
			Casque_Fer c2 = new Casque_Fer();			//
			this.items.add(c2);							// Pour test inventaire
			Casque_Fer c = new Casque_Fer();			//
			this.items.add(c);							//
			toDel = false;								//
		}
		if(this.items.size() < TAILLE){
			this.items.add(item);
			for(int i =  0; i < 19; i++) {  //
				Pomme p = new Pomme("p"+i); // Ligne pour tester plusieurs items dans l'inventaire
				this.items.add(p);			//
			}
			this.navig.actuItem();
			return true;
		}
		return false;
	}
	
	/**
	 * M�thode appell� par le controleur qui initialise l'utilisation d'un item
	 * @param item
	 * @param p
	 */
	public void utiliserItem(Item item, Player p) {
		if(this.items.size() > 0)
			item.utiliser(p);
	}
	
	/**
	 * M�thode qui permet d'�quiper une piece d'armure
	 * @param e
	 */
	public void equipItem(Equipement e) {
		if(this.armure.put(e.getType(),e) != null)  		//Si une pi�ce de ce type d'armure est deja equip�
			this.items.add(this.armure.get(e.getType()));	//Alors on r�cup�re l'ancienne piece dans l'inventaire
		this.armure.put(e.getType(),e);						//Puis on equipe la nouvelle
		this.jeterItem(e);									//Qu'on enleve de l'inventaire
	}
	

	public void draw(SpriteBatch sb) {
		// On dessine l'inventaire
		int ligne = 0;
		int colonne = 0;
		sb.draw(this.sprite,this.pos.getX(),this.pos.getY(), this.sprite.getWidth()*Play.MULTIPLICATEUR, this.sprite.getHeight()*Play.MULTIPLICATEUR);
		
		// On dessine les items de l'inventaire
		for(int i = 0; i < items.size(); i++){
			//220    
			if(i >= 16)
				ligne = 2;
			else
				if(i >= 8)
					ligne = 1;
			sb.draw(this.items.get(i).getSprite(),(213+(colonne*42))*Play.MULTIPLICATEUR,(345-(ligne*40))*Play.MULTIPLICATEUR, this.items.get(i).getSprite().getWidth()*Play.MULTIPLICATEUR, this.items.get(i).getSprite().getHeight()*Play.MULTIPLICATEUR);
			colonne++;
			if(colonne >= 8)
				colonne = 0;
		}	
		this.navig.draw(sb);
		
		sb.draw(this.spriteEquipement, this.pos.getX()+this.sprite.getWidth()+40, this.pos.getY(), this.sprite.getWidth()*Play.MULTIPLICATEUR, this.sprite.getHeight()*Play.MULTIPLICATEUR);
		//On dessine les Equipement �quip�s
		if(this.armure.get("casque") != null)
			sb.draw(this.armure.get("casque").getSprite(),630*Play.MULTIPLICATEUR,380*Play.MULTIPLICATEUR, this.armure.get("casque").getSprite().getWidth()*Play.MULTIPLICATEUR*2, this.armure.get("casque").getSprite().getHeight()*Play.MULTIPLICATEUR*2);
	
		if(this.armure.get("torse") != null)
			sb.draw(this.armure.get("torse").getSprite(),835*Play.MULTIPLICATEUR,312*Play.MULTIPLICATEUR, this.armure.get("torse").getSprite().getWidth()*Play.MULTIPLICATEUR*2, this.armure.get("torse").getSprite().getHeight()*Play.MULTIPLICATEUR*2);
	
		if(this.armure.get("bottes") != null)
			sb.draw(this.armure.get("bottes").getSprite(),820*Play.MULTIPLICATEUR,240*Play.MULTIPLICATEUR, this.armure.get("bottes").getSprite().getWidth()*Play.MULTIPLICATEUR*2, this.armure.get("bottes").getSprite().getHeight()*Play.MULTIPLICATEUR*2);
	
		if(this.armure.get("jambe") != null)
			sb.draw(this.armure.get("jambe").getSprite(),580*Play.MULTIPLICATEUR,260*Play.MULTIPLICATEUR, this.armure.get("jambe").getSprite().getWidth()*Play.MULTIPLICATEUR*2, this.armure.get("jambe").getSprite().getHeight()*Play.MULTIPLICATEUR*2);
	
	}
	
	public void jeterItem() {
		if(this.items.size() > 0)
			this.items.remove(this.items.get(this.navig.getNumItemSelect()));
		
		if(this.items.size() > 0) {
			if(this.navig.getNumItemSelect() > 0)
				if(this.items.get(this.navig.getNumItemSelect()-1) != null)
					this.navig.setNumItemSelect(this.navig.getNumItemSelect()-1);
				else
					this.navig.setNumItemSelect(0);
			this.navig.actuItem();
		}
	}
	
	public void jeterItem(Item i) {
		if(this.items.size() > 0)
			this.items.remove(i);
		
		if(this.items.size() > 0) {
			if(this.navig.getNumItemSelect() > 0)
				if(this.items.get(this.navig.getNumItemSelect()-1) != null)
					this.navig.setNumItemSelect(this.navig.getNumItemSelect()-1);
				else
					this.navig.setNumItemSelect(0);
			this.navig.actuItem();
		}
	}
	
	public boolean getAfficheInventaire(){
		return this.afficheInventaire;
	}
	
	public void setAfficheInventaire(){
	    this.afficheInventaire = !this.afficheInventaire;
	}
	
	public Item getItemPos(int p) {
		if(this.items.size() != 0 && p >= 0) {
			return this.items.get(p);
		}
		else
			return null;
	}
	
	public Item getItemSelect() {
		if(this.items.size() != 0)
			return this.items.get(this.navig.getNumItemSelect());
		else
			return null;
	}
	
	public void navig_DG(int p) {
		if(this.items.size() > 0)
			this.navig.addNum(p);
	}
	
	public int nbItem() {
		return this.items.size();
	}
	
	public Position getPos() {
		return this.pos;
	}
	
	public Sprite getSprite() {
		return this.sprite;
	}
	
	public boolean encoreItem() {
		if (this.items.size() > 0)
			return true;
		else return false;
	}
	
}

