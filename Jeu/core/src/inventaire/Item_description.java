package inventaire;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Items.Item;
import aRanger.Play;
import aRanger.Position;

public class Item_description {
	
	private Position posBoite, posNom, posDesc;
	private String desc, nom;
	private Sprite sprite;
	
	public Item_description(Item i) {
		if(i != null) {
			this.desc = i.getDescription();
			this.nom = i.getName();
		}
		this.posBoite = new Position(200,100);
		this.posBoite.setX(200);
		this.posBoite.setY(50);
		
		this.posNom = new Position(285,180);
		
		this.posDesc = new Position(205, 130);
		
		this.sprite = new Sprite(new Texture("sprite/Inventaire_Item_desc.png"));
	}
	
	public void draw(SpriteBatch sb) {
	
		BitmapFont font = new BitmapFont();

		sb.draw(this.sprite,this.posBoite.getX()*Play.MULTIPLICATEUR,this.posBoite.getY()*Play.MULTIPLICATEUR, this.sprite.getWidth()*Play.MULTIPLICATEUR, this.sprite.getHeight()*Play.MULTIPLICATEUR);
		font.getData().setScale(2);
		font.draw(sb, this.nom, posNom.getX(), posNom.getY());
		if(this.desc != ""  && this.desc != null) {
			font.getData().setScale(1);
			font.draw(sb, this.desc, posDesc.getX(), posDesc.getY());
		}

	}
	
	public void changeItemDesc(Item i) {
		this.desc = i.getDescription();
		this.nom = i.getName();
	}
	
}
