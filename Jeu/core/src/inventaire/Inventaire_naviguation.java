package inventaire;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Items.Item;
import aRanger.Play;

/**
 * Cette classe est utiliser pour la naviguation dans l'inventaire
 * 			Elle permet d'indiquer l'item s�lectionn� 
 * 			Son but est d'all�ger la classe Inventaire
 * @author Florian Corbet
 *
 */
public class Inventaire_naviguation {

	private Item itemSelect;				// Item s�lectionn� actuellement
	private int numItemSelect;				// Index de l'item s�lectionn�
	private Inventaire inventaire;			// Inventaire dans lequel on navigue
	private Sprite sprite;					// Sprite de l'indicateur de naviguation (expl : petit carr�e rouge)
	private Item_description itemDesc;		// Objet permettant l'affichage de la description de l'item
	
	public Inventaire_naviguation(Inventaire i) {
		this.inventaire = i;
		this.numItemSelect = 0;
		this.itemSelect = this.inventaire.getItemPos(this.numItemSelect);
		this.sprite = new Sprite(new Texture("sprite/Inventaire_navig.png"));
		this.itemDesc = new Item_description(this.itemSelect);
	}
	
	/**
	 * M�thode desssinant l'indicateur de naviguation et qui appelle le dessin de la description
	 * 		Est appel� par le dessin de l'Inventaire (draw)
	 * @param sb, SpriteBach la zone de dessin
	 */
	public void draw(SpriteBatch sb) {
		
		//On identifie la position dans l'iventaire de l'Item. 
		//			En haut � gauche 0, en haut � droite 7, en bas � gauche 16, en bad � droite 24
		int colonne = this.numItemSelect; int ligne = 0;
		if(this.numItemSelect >= 16) {
			colonne = this.numItemSelect-16;
			ligne = 2;
		}
		else
			if(this.numItemSelect >= 8) {
				colonne = this.numItemSelect-8;
				ligne = 1;
			}
		float posX = this.inventaire.getSprite().getHeight()-10;
		float posY = this.inventaire.getSprite().getWidth()-10;
		
		//Si il y a au moins un item dans l'iventaire, on dessine l'indicateur 
		if(this.inventaire.encoreItem()) {
			sb.draw(this.sprite,(posX+(colonne*42))*Play.MULTIPLICATEUR,(posY-(ligne*40))*Play.MULTIPLICATEUR, this.sprite.getWidth()*Play.MULTIPLICATEUR, this.sprite.getHeight()*Play.MULTIPLICATEUR);
			this.itemDesc.draw(sb);			//On appelle aussi la m�thode draw qui va dessiner la description de l'item selectionn�
		}
	}
	
	/**
	 * Methode pour changer le num de l'item select
	 * @param n
	 */
	public void addNum(int n) {
		if(this.numItemSelect+n >= 0 && this.numItemSelect+n <= this.inventaire.nbItem())
		if((n < 0 && this.numItemSelect-n >= 0) ||(n >= 0 && this.numItemSelect+n < this.inventaire.nbItem()))
			this.numItemSelect += n;
		//Quand on change le num de l'item selectionne, on change l'item selectionne et sa description
		this.itemSelect = this.inventaire.getItemPos(this.numItemSelect);
		this.itemDesc.changeItemDesc(this.inventaire.getItemPos(this.numItemSelect));
	}
	
	public int getNumItemSelect() {
		return this.numItemSelect;
	}
	
	public void setNumItemSelect(int i) {
		this.numItemSelect = i;
		this.actuItem();
	}
	
	/**
	 * M�thode qui met � jour l'indicateur et la description en fonction du num�ro de l'item select, si il y a encore un item
	 */
	public void actuItem() {
		if(this.inventaire.encoreItem())
			this.itemSelect = this.inventaire.getItemPos(this.numItemSelect);
			this.itemDesc.changeItemDesc(this.inventaire.getItemPos(this.numItemSelect));
	}
	
}


