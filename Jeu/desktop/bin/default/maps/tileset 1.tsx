<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="tileset 1" tilewidth="32" tileheight="32" spacing="1" tilecount="104" columns="13">
 <image source="32x32_map_tile v1.0.png" width="428" height="263"/>
 <tile id="4">
  <properties>
   <property name="blocked" value=""/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="blocked" value=""/>
   <property name="test" value=""/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="blocked" value=""/>
   <property name="test" value=""/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="blocked" value=""/>
  </properties>
 </tile>
</tileset>
